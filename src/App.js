import React, { useState } from "react"
import TodoForm from "./components/TodoForm"
import TodoList from "./components/TodoList"
let initialTodos = [
  { text: "First", done: true },
  { text: "Second", done: true },
  { text: "Third", done: false },
]
export default function App() {
  let [todos, setTodos] = useState(initialTodos)
  function createTodo({ text }) {
    let todo = { text, done: false }
    setTodos(
      [...todos, todo]
    )
  }
  function removeTodo(text) {
    setTodos(
      todos.filter(todo => todo.text !== text)
    )
  }
  function toggleTodo(text) {
    setTodos(
      todos.map(todo => ({
        ...todo,
        done: todo.text === text ? !todo.done : todo.done
      }))
    )
  }
  return <div className="p-3 container">
    <h2>Create TODO</h2>
    <TodoForm createTodo={createTodo} />
    <div className="mt-4">
      <TodoList todos={todos} toggleTodo={toggleTodo} removeTodo={removeTodo} />
    </div>
  </div>
}